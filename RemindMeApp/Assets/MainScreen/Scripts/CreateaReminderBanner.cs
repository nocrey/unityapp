﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateaReminderBanner : MonoBehaviour {

    public ReminderManager reminderManager;

    public void ConfirmationButtonTapped()
    {
        if(GetComponentInChildren<InputField>().text == "")
        {

        }
        else
        {
            reminderManager.AddReminderLabel(GetComponentInChildren<InputField>().text);
            GetComponentInChildren<InputField>().text = "";
            this.gameObject.SetActive(false);
        }
        
    }
    public void CancelButtonTapped()
    {
        GetComponentInChildren<InputField>().text = "";
        this.gameObject.SetActive(false);
    }
}
