﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ReminderManager : MonoBehaviour {

    public GameObject reminderLabelPrefab;

	// Use this for initialization
	void Start () {
        LoadLabels();
        //SetHeightOfGameobject();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("p"))
        {
            SaveLabels();
        }
	}

    public void AddReminderLabel(string _descriptionReminder)
    {
        if (GetNumberReminders() < 10)
        {
            GameObject newLabel = Instantiate(reminderLabelPrefab, this.transform);
            newLabel.GetComponent<ReminderLabel>().SetData(_descriptionReminder);

            SetHeightOfGameobject();
        }
    }

    public void LoadReminderLabel(Reminder _reminder)
    {
        GameObject newLabel = Instantiate(reminderLabelPrefab, this.transform);
        newLabel.GetComponent<ReminderLabel>().LoadData(_reminder);

        SetHeightOfGameobject();
    }

    int GetNumberReminders()
    {
        return GetComponentsInChildren<ReminderLabel>().Length;
    }

    public void SetHeightOfGameobject()
    {
        GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 310*GetNumberReminders());
        SetPositionLabels();
        SaveLabels();

    }

    void SetPositionLabels()
    {
        int positionsFilled = 0;
        ReminderLabel[] reminderLabelArray = GetComponentsInChildren<ReminderLabel>();
        for (int i = 0; i < reminderLabelArray.Length; i++)
        {
            if(reminderLabelArray[i].GetComponent<AddReminderLabel>() != null)
            {
                reminderLabelArray[i].SetHeight(reminderLabelArray.Length-1);
            }
            else
            {
                reminderLabelArray[i].SetHeight(positionsFilled);
                positionsFilled++;
            }
            
        }
    }

    public void SaveLabels()
    {
        if (File.Exists(Application.persistentDataPath + "/saveData.dat"))
        {
            File.Delete(Application.persistentDataPath + "/saveData.dat");
        }
        
            BinaryFormatter binFor = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/saveData.dat");
            Reminder[] reminders = new Reminder[GetComponentsInChildren<ReminderLabel>().Length -1];

            ReminderLabel[] reminderLabels = GetComponentsInChildren<ReminderLabel>();
            int positionsFilled = 0;
            for (int i = 0; i < reminderLabels.Length; i++)
            {
                if(reminderLabels[i].GetReminder() != null)
                {
                    reminders[positionsFilled] = reminderLabels[i].GetReminder();
                    positionsFilled++;
                }
            }

            binFor.Serialize(file, reminders);
            file.Close();
    }
 
    public void LoadLabels()
    {
        if (File.Exists(Application.persistentDataPath + "/saveData.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/saveData.dat", FileMode.Open);

            Reminder[] reminders = (Reminder[])bf.Deserialize(file);

            file.Close();
            if (reminders.Length > 0)
            {
                for (int i = 0; i < reminders.Length; i++)
                {
                    LoadReminderLabel(reminders[i]);
                }
            }

        } 
    }
}
