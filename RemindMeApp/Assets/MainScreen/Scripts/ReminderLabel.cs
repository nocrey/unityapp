﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReminderLabel : MonoBehaviour {
    Reminder reminder;
    string description;
    public bool active;
    public Animator sliderAnimator;

    public void SetHeight(int positionInArray)
    {
        transform.localPosition = new Vector3(transform.localPosition.x, (positionInArray * -310), 0);
        
    }
    public void SetData(string _description)
    {
        description = _description;
        GetComponentInChildren<Text>().text = description;
        reminder = new Reminder(description);
    }

    public void LoadData(Reminder _reminder)
    {
        reminder = _reminder;
        description = reminder.description;
        GetComponentInChildren<Text>().text = description;
        
        if (reminder.activationTime.Day == DateTime.Today.Day)
        {
            active = reminder.active;
            sliderAnimator.SetBool("IsActive", active);
        }
        else
        {
            active = false;
            reminder.activationTime = DateTime.Today;
            GetComponentInParent<ReminderManager>().SaveLabels();
        }
    }

    public Reminder GetReminder()
    {
        return reminder;
    }

    public void SwitchActive()
    {
        active = !active;
        if (reminder != null)
        {
            reminder.active = active;
        }

        sliderAnimator.SetBool("IsActive", active);
        GetComponentInParent<ReminderManager>().SaveLabels();
    }

}

[Serializable]
public class Reminder
{
    public string description;
    public bool active;
    public DateTime activationTime;

    public Reminder(string _description)
    {
        description = _description;
        active = false;
        activationTime = DateTime.Now;
    }
}
