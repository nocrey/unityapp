﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteLabel : MonoBehaviour {


    public void Delete()
    {
        DestroyImmediate(GetComponent<ReminderLabel>());
        GetComponentInParent<ReminderManager>().SetHeightOfGameobject();
        Destroy(gameObject);
    }
}
