﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddReminderLabel : MonoBehaviour {
    public GameObject createReminderBanner;

    public void ActivateCreationBanner()
    {
        createReminderBanner.SetActive(true);
    }
}
